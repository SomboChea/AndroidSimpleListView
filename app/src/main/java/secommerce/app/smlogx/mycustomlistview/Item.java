package secommerce.app.smlogx.mycustomlistview;

import java.io.Serializable;

public class Item implements Serializable {

    private int Image;
    private String Title;
    private String Size;
    private String Date;

    public Item() {}
    public Item(int image, String title, String size, String date) {
        this.Image = image;
        this.Title = title;
        this.Size = size;
        this.Date = date;
    }
    public int getImage() {
        return Image;
    }

    public void setImage(int image) {
        Image = image;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getSize() {
        return Size;
    }

    public void setSize(String size) {
        Size = size;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }
}
