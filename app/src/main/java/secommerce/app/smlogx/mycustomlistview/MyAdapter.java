package secommerce.app.smlogx.mycustomlistview;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class MyAdapter extends BaseAdapter {

    private Activity context;
    private ArrayList<Item> items;

    public MyAdapter() {}

    public MyAdapter(Activity _context, ArrayList<Item> _items) {
        this.context = _context;
        this.items = _items;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return items.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    class ViewHolder {
        public ImageView image;
        public TextView txtTitle, txtSize, txtDate;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup parent) {

        View rowView = convertView;

        if(rowView ==null) {
            LayoutInflater inflater = context.getLayoutInflater();
            rowView = inflater.inflate(R.layout.row_layout, null);
            ViewHolder holder = new ViewHolder();
            holder.image = rowView.findViewById(R.id.imgView);
            holder.txtTitle = rowView.findViewById(R.id.txtTitle);
            holder.txtSize = rowView.findViewById(R.id.txtSize);
            holder.txtDate = rowView.findViewById(R.id.txtDate);

            rowView.setTag(holder);
        }

        ViewHolder viewHolder = (ViewHolder) rowView.getTag();
        String title,size,date;
        int imgResTd;

        imgResTd = items.get(i).getImage();
        title = items.get(i).getTitle();
        size = items.get(i).getSize();
        date = items.get(i).getDate();

        viewHolder.txtTitle.setText(title);
        viewHolder.txtSize.setText(size);
        viewHolder.txtDate.setText(date);
        viewHolder.image.setImageResource(imgResTd);

        return rowView;
    }
}
