package secommerce.app.smlogx.mycustomlistview;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class Item_Detail extends AppCompatActivity {

    private ImageView imgView;
    private TextView txtItemTitle, txtItemSize, txtItemDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item__detail);

        imgView = findViewById(R.id.imgView);
        txtItemTitle = findViewById(R.id.txtItemTitle);
        txtItemSize = findViewById(R.id.txtItemSize);
        txtItemDate = findViewById(R.id.txtItemDate);

        Item item = (Item) getIntent().getSerializableExtra("data");

        imgView.setImageResource(item.getImage());
        txtItemTitle.setText(item.getTitle());
        txtItemSize.setText(item.getSize());
        txtItemDate.setText(item.getDate());

    }
}
