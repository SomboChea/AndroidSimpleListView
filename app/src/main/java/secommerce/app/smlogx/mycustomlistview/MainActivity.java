package secommerce.app.smlogx.mycustomlistview;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.Serializable;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        final ArrayList<Item> items = new ArrayList<>();

        items.add(new Item(R.drawable.word, "Word", "5MB","03/3/2018"));
        items.add(new Item(R.drawable.excel, "Excel", "10MB","03/1/2018"));
        items.add(new Item(R.drawable.access, "Access", "5.08MB","03/2/2018"));
        items.add(new Item(R.drawable.office, "Office", "50MB","03/4/2018"));
        items.add(new Item(R.drawable.gg, "Google", "40MB","03/3/2018"));
        items.add(new Item(R.drawable.photo, "Photo", "11MB","03/3/2018"));
        items.add(new Item(R.drawable.gplus, "Google Plus", "6MB","03/3/2018"));

        MyAdapter adapter = new MyAdapter(this, items);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, Item_Detail.class);
                intent.putExtra("data",items.get(position));
                Toast.makeText(MainActivity.this, items.get(position).getTitle(), Toast.LENGTH_SHORT).show();
                startActivity(intent);
            }
        });
    }
}
